package uml.edu.sparkcl;

import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.api.java.JavaRDDLike;
import java.util.ArrayList;
import java.util.Iterator;


public class SparkUtil
{
	public static <T> SparkCL<T> genSparkCL(JavaRDDLike<T,?> data)
	{
		return new SparkCL<T>(data);
	}
	
	public static <T> ArrayList<T> getArrayFromIterator(Iterator<T> dataListItr)
	{
  	  final ArrayList<T> dataList = new ArrayList<T>();

  	  while(dataListItr.hasNext())
  	  {
  		dataList.add(dataListItr.next());
  	  }
  	  return dataList;
	}

	// Unfortunately generics in Java (Unlike C++ for example) do not allow to use simple types in templates:
	// https://www.java.net/node/643733
	// so below are some ugly special purpose functions...
	public static int[] intArrayFromIterator(Iterator<Integer> dataListItr)
	{
          ArrayList<Integer> tempArray= SparkUtil.getArrayFromIterator(dataListItr);
	  	  Integer[] integerArray = new Integer[tempArray.size()];
  	      tempArray.toArray(integerArray);
          return ArrayUtils.toPrimitive(integerArray);
	}

	/*
	public static float[] floatArrayFromIterator(Iterator<Float> dataListItr)
	{
        //Float [] temp = (Float[]) SparkUtil.getArrayFromIterator(dataListItr);
        return ArrayUtils.toPrimitive(SparkUtil.getArrayFromIterator(dataListItr));
	}
    */
	
	public static Iterator<Integer> intArrayToIterator(final int [] primitiveArray)
	{
		java.lang.Iterable<Integer> aIterable=new Iterable<Integer>() {

		    public Iterator<Integer> iterator() {
		       return new Iterator<Integer>() {
		            private int pos=0;

		            public boolean hasNext() {
		               return primitiveArray.length>pos;
		            }

		            public Integer next() {
		               return primitiveArray[pos++];
		            }

		            public void remove() {
		                throw new UnsupportedOperationException("Cannot remove an element of an array.");
		            }
		        };
		    }
		};
		return aIterable.iterator();	
	}
	

	public static java.lang.reflect.Method getMethodByName(String className, String methodName, Class[] argTypes) 
	{
		java.lang.reflect.Method method = null;
		try {
		    Class<?> c = Class.forName(className);
		    method = c.getDeclaredMethod(methodName, argTypes);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    return method;
	}
/*	
    public abstract class SparkKernel<T> extends Kernel implements Serializable
	{

		*//**
		 * 
		 *//*
		private static final long serialVersionUID = 834952053903186797L;
		
		Range range;
		
		public abstract void mapParams(T data);
		//{
			//float a[] = data.a;
			//float b[] = data.b;
			//float c[] = data.c;
		//}
		public abstract void run(); 	

		public abstract T mapReturn(T data);

	}
*/	
/*	
    public class SparkCL<T>
	{
		// data
		JavaRDD<T> m_data;

		public SparkCL(JavaRDD<T> data) 
		{
			m_data = data;
		}

		public JavaRDD<T> mapCL(final SparkKernel<T> kernel) //throws Exception//KernelFunction kf)
		{
			return (JavaRDD<T>) m_data.map(new Function<T,T>()
					{

				@Override
				public T call(T v1) throws Exception 
				{
					// TODO Auto-generated method stub
					kernel.mapParams(v1);

					final long KernelTimeOutMill = 60 * 1000; 
					try (ProcessSync.ExclusiveAccess exls= new ProcessSync.ExclusiveAccess("Kernel.Process.Sync.file",KernelTimeOutMill);)
					{
						kernel.execute(kernel.range);		
					}

					return kernel.mapReturn(v1);
				}

					});
			// T or javaRDD<T>

			//kernel.execute(Range.create(c.length));
			//kernel.execute(range);		
		}
	}
*/		
   //public class JavaRDDCL<T,U extends Number> extends  JavaRDD<T>

/*   
   public class JavaRDDCL<T> extends  JavaRDD<T>
   {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 6551041141711996584L;

	public JavaRDDCL(RDD<T> rdd, ClassTag<T> classTag) 
	{
		super(rdd, classTag);
		// TODO Auto-generated constructor stub
		//this.cache();
		
	}
	
	
	//ArrayList<Object> paramArray;
	
	//public void mapParam(Object x) 
	//{
	//	paramArray.add(x);
	//}
	
	//U a[];
	//U b[];
	//U c[];
	
	//public void mapParams()
	//{
		//float a[] = x.a;
		//float b[] = x.b;
		//float c[] = x.c;
	//}
	
	//public JavaRDDCL<T,U> execCL(Kernel kernel)//Kernel kernel)//KernelFunction kf)
	//public JavaRDDCL<T,U> execCL(U a[], U b[], U c[])//Kernel kernel)//KernelFunction kf)
	//public JavaRDDCL<T> execCL(SparkKernel<T> kernel) throws Exception//KernelFunction kf)
	public JavaRDDCL<T> execCL(final SparkKernel<T> kernel) throws Exception//KernelFunction kf)
	{
		
		//Kernel kernel = new Kernel();
		//Kernel kernel = new Kernel(){
		//	@Override public void run() {
		//		int gid = getGlobalId();
		//		c[gid] = a[gid] + b[gid];
		//	}
		//};

		return (JavaRDDCL<T>) map(new Function<T,T>()
		{

			@Override
			public T call(T v1) throws Exception 
			{
				// TODO Auto-generated method stub
				kernel.mapParams(v1);
				
				final long KernelTimeOutMill = 60 * 1000; 
				try (ProcessSync.ExclusiveAccess exls= new ProcessSync.ExclusiveAccess("Kernel.Process.Sync.file",KernelTimeOutMill);)
				{
						kernel.execute(kernel.range);		
		  	    }

				return kernel.mapReturn(v1);
			}
			
		});
		// T or javaRDD<T>

		//kernel.execute(Range.create(c.length));
		//kernel.execute(range);		
	  }
   }
*/   
}
